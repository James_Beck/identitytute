﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using IdentityProto1.Models;
using IdentityProto1.Models.AccountViewModels;

namespace IdentityProto1.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options){ }

        public DbSet<LoginViewModel> LoginViewModels { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<LoginViewModel>().ToTable("LoginViewModel");
        }
    }
}
